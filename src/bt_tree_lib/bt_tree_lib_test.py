#!/usr/bin/env python


import unittest
from bt_tree_lib import *


class TestNodeStatus(unittest.TestCase):
    def test_status(self):
        self.assertEquals((NodeStatus.SUCCESS, NodeStatus.RUNNING, NodeStatus.FAILURE), (0, 1, 2))


class TestNode(unittest.TestCase):
    def test_init(self):
        NODE = Node("NODE")
        self.assertEquals((NODE.name, NODE.type, NODE.status), ("NODE", None, None))

    def test_run(self):
        pass

    def test_reset(self):
        NODE = Node("NODE")
        NODE.status = NodeStatus.SUCCESS
        NODE.reset()
        self.assertEquals(NODE.status, None)


class TestLeafNode(unittest.TestCase):
    def test_init(self):
        LEAFNODE = LeafNode("LEAFNODE")
        self.assertEquals(LEAFNODE.type, "Leaf")


class LeafNode_SUCCESS(LeafNode):
    def run(self):
        return NodeStatus.SUCCESS


class LeafNode_RUNNING(LeafNode):
    def run(self):
        return NodeStatus.RUNNING


class LeafNode_FAILURE(LeafNode):
    def run(self):
        return NodeStatus.FAILURE


class TestInternalNode(unittest.TestCase):
    def test_init(self):
        LEAFNODE_SUCCESS = LeafNode_SUCCESS("LEAFNODE_SUCCESS")
        LEAFNODE_RUNNING = LeafNode_RUNNING("LEAFNODE_RUNNING")
        LEAFNODE_FAILURE = LeafNode_FAILURE("LEAFNODE_FAILURE")
        INTERNALNODE = InternalNode("INTERNALNODE", [LEAFNODE_SUCCESS, LEAFNODE_RUNNING, LEAFNODE_FAILURE])
        self.assertEquals(INTERNALNODE.type, "Internal")
        self.assertEquals(INTERNALNODE.children, [LEAFNODE_SUCCESS, LEAFNODE_RUNNING, LEAFNODE_FAILURE])

    def test_reset(self):
        LEAFNODE_SUCCESS = LeafNode_SUCCESS("LEAFNODE_SUCCESS")
        LEAFNODE_RUNNING = LeafNode_RUNNING("LEAFNODE_RUNNING")
        LEAFNODE_FAILURE = LeafNode_FAILURE("LEAFNODE_FAILURE")
        INTERNALNODE = InternalNode("INTERNALNODE", [LEAFNODE_SUCCESS, LEAFNODE_RUNNING, LEAFNODE_FAILURE])
        LEAFNODE_SUCCESS.status, LEAFNODE_RUNNING.status, LEAFNODE_FAILURE.status = NodeStatus.SUCCESS, NodeStatus.RUNNING, NodeStatus.FAILURE
        INTERNALNODE.reset()
        self.assertEquals((LEAFNODE_SUCCESS.status, LEAFNODE_RUNNING.status, LEAFNODE_FAILURE.status), (None, None, None))


class TestInternalNode_withMemory(unittest.TestCase):
    def test_init(self):
        LEAFNODE_SUCCESS = LeafNode_SUCCESS("LEAFNODE_SUCCESS")
        LEAFNODE_RUNNING = LeafNode_RUNNING("LEAFNODE_RUNNING")
        LEAFNODE_FAILURE = LeafNode_FAILURE("LEAFNODE_FAILURE")
        INTERNALNODE_WITHMEMORY = InternalNode_withMemory("INTERNALNODE_WITHMEMORY", [LEAFNODE_SUCCESS, LEAFNODE_RUNNING, LEAFNODE_FAILURE])
        self.assertEquals(INTERNALNODE_WITHMEMORY.type, "Internal_M")
        self.assertEquals(INTERNALNODE_WITHMEMORY.current, 0)

    def test_reset(self):
        LEAFNODE_SUCCESS = LeafNode_SUCCESS("LEAFNODE_SUCCESS")
        LEAFNODE_RUNNING = LeafNode_RUNNING("LEAFNODE_RUNNING")
        LEAFNODE_FAILURE = LeafNode_FAILURE("LEAFNODE_FAILURE")
        INTERNALNODE_WITHMEMORY = InternalNode_withMemory("INTERNALNODE_WITHMEMORY", [LEAFNODE_SUCCESS, LEAFNODE_RUNNING, LEAFNODE_FAILURE])
        INTERNALNODE_WITHMEMORY.current = 1
        INTERNALNODE_WITHMEMORY.reset()
        self.assertEquals(INTERNALNODE_WITHMEMORY.current, 0)


class TestDecoratorNode(unittest.TestCase):
    def test_init(self):
        LEAFNODE_SUCCESS = LeafNode_SUCCESS("LEAFNODE_SUCCESS")
        DECORATORNODE = DecoratorNode("DECORATORNODE", LEAFNODE_SUCCESS)

        self.assertEquals(DECORATORNODE.type, "Decorator")
        self.assertEquals(DECORATORNODE.child, LEAFNODE_SUCCESS)

    def test_reset(self):
        LEAFNODE_SUCCESS = LeafNode_SUCCESS("LEAFNODE_SUCCESS")
        DECORATORNODE = DecoratorNode("DECORATORNODE", LEAFNODE_SUCCESS)

        LEAFNODE_SUCCESS.status = NodeStatus.SUCCESS
        DECORATORNODE.reset()
        self.assertEquals(LEAFNODE_SUCCESS.status, None)


class TestSelector(unittest.TestCase):
    def test_init(self):
        LEAFNODE_SUCCESS = LeafNode_SUCCESS("LEAFNODE_SUCCESS")
        LEAFNODE_RUNNING = LeafNode_RUNNING("LEAFNODE_RUNNING")
        LEAFNODE_FAILURE = LeafNode_FAILURE("LEAFNODE_FAILURE")
        SELECTOR = Selector("SELECTOR", [LEAFNODE_SUCCESS, LEAFNODE_RUNNING, LEAFNODE_FAILURE])
        self.assertEquals(SELECTOR.type, "Selector")

    def test_run(self):
        LEAFNODE_SUCCESS = LeafNode_SUCCESS("LEAFNODE_SUCCESS")
        LEAFNODE_RUNNING = LeafNode_RUNNING("LEAFNODE_RUNNING")
        LEAFNODE_FAILURE = LeafNode_FAILURE("LEAFNODE_FAILURE")

        SELECTOR = Selector("SELECTOR", [LEAFNODE_FAILURE, LEAFNODE_SUCCESS])
        self.assertEquals(SELECTOR.run(), NodeStatus.SUCCESS)
        SELECTOR = Selector("SELECTOR", [LEAFNODE_FAILURE, LEAFNODE_RUNNING])
        self.assertEquals(SELECTOR.run(), NodeStatus.RUNNING)
        SELECTOR = Selector("SELECTOR", [LEAFNODE_FAILURE, LEAFNODE_FAILURE])
        self.assertEquals(SELECTOR.run(), NodeStatus.FAILURE)


class TestSequence(unittest.TestCase):
    def test_init(self):
        LEAFNODE_SUCCESS = LeafNode_SUCCESS("LEAFNODE_SUCCESS")
        LEAFNODE_RUNNING = LeafNode_RUNNING("LEAFNODE_RUNNING")
        LEAFNODE_FAILURE = LeafNode_FAILURE("LEAFNODE_FAILURE")
        SEQUENCE = Sequence("SEQUENCE", [LEAFNODE_SUCCESS, LEAFNODE_RUNNING, LEAFNODE_FAILURE])
        self.assertEquals(SEQUENCE.type, "Sequence")

    def test_run(self):
        LEAFNODE_SUCCESS = LeafNode_SUCCESS("LEAFNODE_SUCCESS")
        LEAFNODE_RUNNING = LeafNode_RUNNING("LEAFNODE_RUNNING")
        LEAFNODE_FAILURE = LeafNode_FAILURE("LEAFNODE_FAILURE")

        SEQUENCE = Sequence("SEQUENCE", [LEAFNODE_SUCCESS, LEAFNODE_SUCCESS])
        self.assertEquals(SEQUENCE.run(), NodeStatus.SUCCESS)
        SEQUENCE = Sequence("SEQUENCE", [LEAFNODE_SUCCESS, LEAFNODE_RUNNING])
        self.assertEquals(SEQUENCE.run(), NodeStatus.RUNNING)
        SEQUENCE = Sequence("SEQUENCE", [LEAFNODE_SUCCESS, LEAFNODE_FAILURE])
        self.assertEquals(SEQUENCE.run(), NodeStatus.FAILURE)


class TestSelector_withMemory(unittest.TestCase):
    def test_init(self):
        LEAFNODE_SUCCESS = LeafNode_SUCCESS("LEAFNODE_SUCCESS")
        LEAFNODE_RUNNING = LeafNode_RUNNING("LEAFNODE_RUNNING")
        LEAFNODE_FAILURE = LeafNode_FAILURE("LEAFNODE_FAILURE")
        SELECTOR_WITHMEMORY = Selector_withMemory("SELECTOR_WITHMEMORY", [LEAFNODE_SUCCESS, LEAFNODE_RUNNING, LEAFNODE_FAILURE])
        self.assertEquals(SELECTOR_WITHMEMORY.type, "Selector_M")

    def test_run(self):
        LEAFNODE_SUCCESS = LeafNode_SUCCESS("LEAFNODE_SUCCESS")
        LEAFNODE_RUNNING = LeafNode_RUNNING("LEAFNODE_RUNNING")
        LEAFNODE_FAILURE = LeafNode_FAILURE("LEAFNODE_FAILURE")

        SELECTOR_WITHMEMORY = Selector_withMemory("SELECTOR_WITHMEMORY", [LEAFNODE_FAILURE, LEAFNODE_SUCCESS])
        self.assertEquals(SELECTOR_WITHMEMORY.run(), NodeStatus.SUCCESS)
        self.assertEquals(SELECTOR_WITHMEMORY.current, 1)
        SELECTOR_WITHMEMORY = Selector_withMemory("SELECTOR_WITHMEMORY", [LEAFNODE_FAILURE, LEAFNODE_RUNNING])
        self.assertEquals(SELECTOR_WITHMEMORY.run(), NodeStatus.RUNNING)
        self.assertEquals(SELECTOR_WITHMEMORY.current, 1)
        SELECTOR_WITHMEMORY = Selector_withMemory("SELECTOR_WITHMEMORY", [LEAFNODE_FAILURE, LEAFNODE_FAILURE])
        self.assertEquals(SELECTOR_WITHMEMORY.run(), NodeStatus.FAILURE)
        self.assertEquals(SELECTOR_WITHMEMORY.current, 0)


class TestSequence_withMemory(unittest.TestCase):
    def test_init(self):
        LEAFNODE_SUCCESS = LeafNode_SUCCESS("LEAFNODE_SUCCESS")
        LEAFNODE_RUNNING = LeafNode_RUNNING("LEAFNODE_RUNNING")
        LEAFNODE_FAILURE = LeafNode_FAILURE("LEAFNODE_FAILURE")
        SEQUENCE_WITHMEMORY = Sequence_withMemory("SEQUENCE_WITHMEMORY", [LEAFNODE_SUCCESS, LEAFNODE_RUNNING, LEAFNODE_FAILURE])
        self.assertEquals(SEQUENCE_WITHMEMORY.type, "Sequence_M")

    def test_run(self):
        LEAFNODE_SUCCESS = LeafNode_SUCCESS("LEAFNODE_SUCCESS")
        LEAFNODE_RUNNING = LeafNode_RUNNING("LEAFNODE_RUNNING")
        LEAFNODE_FAILURE = LeafNode_FAILURE("LEAFNODE_FAILURE")

        SEQUENCE_WITHMEMORY = Sequence_withMemory("SEQUENCE_WITHMEMORY", [LEAFNODE_SUCCESS, LEAFNODE_SUCCESS])
        self.assertEquals(SEQUENCE_WITHMEMORY.run(), NodeStatus.SUCCESS)
        self.assertEquals(SEQUENCE_WITHMEMORY.current, 0)
        SEQUENCE_WITHMEMORY = Sequence_withMemory("SEQUENCE_WITHMEMORY", [LEAFNODE_SUCCESS, LEAFNODE_RUNNING])
        self.assertEquals(SEQUENCE_WITHMEMORY.run(), NodeStatus.RUNNING)
        self.assertEquals(SEQUENCE_WITHMEMORY.current, 1)
        SEQUENCE_WITHMEMORY = Sequence_withMemory("SEQUENCE_WITHMEMORY", [LEAFNODE_SUCCESS, LEAFNODE_FAILURE])
        self.assertEquals(SEQUENCE_WITHMEMORY.run(), NodeStatus.FAILURE)
        self.assertEquals(SEQUENCE_WITHMEMORY.current, 1)


class TestDecoratorNode_NOT(unittest.TestCase):
    def test_init(self):
        LEAFNODE_SUCCESS = LeafNode_SUCCESS("LEAFNODE_SUCCESS")
        DECORATORNODE_NOT = DecoratorNode_NOT("DECORATORNODE_NOT", LEAFNODE_SUCCESS)

        self.assertEquals(DECORATORNODE_NOT.type, "Decorator_NOT")

    def test_run(self):
        LEAFNODE_SUCCESS = LeafNode_SUCCESS("LEAFNODE_SUCCESS")
        LEAFNODE_RUNNING = LeafNode_RUNNING("LEAFNODE_RUNNING")
        LEAFNODE_FAILURE = LeafNode_FAILURE("LEAFNODE_FAILURE")

        DECORATORNODE_NOT = DecoratorNode_NOT("DECORATORNODE_NOT", LEAFNODE_SUCCESS)
        self.assertEquals(DECORATORNODE_NOT.run(), NodeStatus.FAILURE)
        DECORATORNODE_NOT = DecoratorNode_NOT("DECORATORNODE_NOT", LEAFNODE_RUNNING)
        self.assertEquals(DECORATORNODE_NOT.run(), NodeStatus.RUNNING)
        DECORATORNODE_NOT = DecoratorNode_NOT("DECORATORNODE_NOT", LEAFNODE_FAILURE)
        self.assertEquals(DECORATORNODE_NOT.run(), NodeStatus.SUCCESS)


if __name__ == "__main__":
    pass


