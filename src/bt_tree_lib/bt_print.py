#!/usr/bin/env python


from bt_tree_lib import *


_LSet = {"Leaf"}
_ISet = {"Selector", "Sequence", "Selector_M", "Sequence_M"}
_DSet = {"Decorator_NOT"}


def bt_list(bt_tree):
    prefix = {'name': bt_tree.name, 'type': bt_tree.type, 'status': bt_tree.status}
    node_list = [prefix]

    if bt_tree.type in _LSet:
        pass

    if bt_tree.type in _ISet:
        for node in bt_tree.children:
            node_list.append(bt_list(node))

    if bt_tree.type in _DSet:
        node_list.append(bt_list(bt_tree.child))

    return node_list


_NodeStatus_Map = {None: "None",
    0: "SUCCESS", 1: "RUNNING", 2: "FAILURE"}
_string_prefix, _string_list = [], []


def _bt_node_string(node_list):
    node_string_prefix = ""
    for item in _string_prefix:
        node_string_prefix = node_string_prefix + item

    return node_string_prefix + str(node_list[0]['name']) + "," + str(node_list[0]['type']) + "," + _NodeStatus_Map[node_list[0]['status']]


def bt_string(node_list):
    _string_list.append(_bt_node_string(node_list))

    _string_prefix.append("--")
    for index in range(1, len(node_list)):
        bt_string(node_list[index])
    _string_prefix.pop()

    return _string_list


def bt_string_clear():
    del _string_list[:]
    return


if __name__ == "__main__":
    pass

