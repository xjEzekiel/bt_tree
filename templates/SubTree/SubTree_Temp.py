#!/usr/bin/env python


_prefix = "Temp"


import rospy


Temp_SubTree_Dict = {}


class Temp_SubTreeParamStatus(object):
    PREPARING = 0
    EXECUTING = 1


class Temp_SubTreeParam(object):
    def __init__(self, *args, **kwargs):
        super(Temp_SubTreeParam, self).__init__(*args, **kwargs)
        self.status = Temp_SubTreeParamStatus.PREPARING
        return

    def get_status(self):
        return self.status

    def set_status(self, status):
        self.status = status
        return

    def reset(self):
        self.status = Temp_SubTreeParamStatus.PREPARING
        return

    def set(self, param):
        return


def Temp_init():
    return


def Temp_cancel():
    for key, value in Temp_SubTree_Dict.items():
        value['SubTreeParam'].reset()
        value['SubTree'].reset()
    return


def Temp_get_SubTree(name):
    # Input check.
    if not isinstance(name, str):
        return False, None, None, "Parameter passed is not a string."
    if Temp_SubTree_Dict.has_key(name):
        return False, None, None, "The Temp_SubTree %s has already existed." % name
    
    SubTreeParam = Temp_SubTreeParam()
    SubTree = _Temp_get_SubTree(SubTreeParam)
    
    # Save the SubTree, and return result.
    Temp_SubTree_Dict[name] = {'SubTreeParam': SubTreeParam, 'SubTree': SubTree}
    return True, SubTreeParam, SubTree, ""


def _Temp_get_SubTree(param):
    return LeafNode("Temp")


class TempNode_has_Temp_EXECUTING(LeafNode):
    def __init__(self, name, param, *args, **kwargs):
        super(TempNode_has_Temp_EXECUTING, self).__init__(name, *args, **kwargs)
        self.param = param
        return

    def run(self):
        status = self.param.get_status()
        if status == Temp_SubTreeParamStatus.EXECUTING:
            return NodeStatus.SUCCESS
        return NodeStatus.FAILURE


class TempNode_set_Temp_EXECUTING(LeafNode):
    def __init__(self, name, param, *args, **kwargs):
        super(TempNode_set_Temp_EXECUTING, self).__init__(name, *args, **kwargs)
        self.param = param
        return

    def run(self):
        self.param.set_status(Temp_SubTreeParamStatus.EXECUTING)
        return NodeStatus.SUCCESS


class TempNode_set_Temp_PREPARING(LeafNode):
    def __init__(self, name, param, *args, **kwargs):
        super(TempNode_set_Temp_PREPARING, self).__init__(name, *args, **kwargs)
        self.param = param
        return

    def run(self):
        self.param.set_status(Temp_SubTreeParamStatus.PREPARING)
        return NodeStatus.SUCCESS






