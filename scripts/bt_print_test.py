#!/usr/bin/env python


from bt_tree_lib.bt_print import *


class LeafNode_SUCCESS(LeafNode):
    def run(self):
        return NodeStatus.SUCCESS


class LeafNode_RUNNING(LeafNode):
    def run(self):
        return NodeStatus.RUNNING


class LeafNode_FAILURE(LeafNode):
    def run(self):
        return NodeStatus.FAILURE


if __name__ == "__main__":
    LEAFNODE_SUCCESS = LeafNode_SUCCESS("LEAFNODE_SUCCESS")
    LEAFNODE_RUNNING = LeafNode_RUNNING("LEAFNODE_RUNNING")
    LEAFNODE_FAILURE = LeafNode_FAILURE("LEAFNODE_FAILURE")

    SEQUENCE_WITHMEMORY = Sequence_withMemory("SEQUENCE_WITHMEMORY", [LEAFNODE_SUCCESS, LEAFNODE_RUNNING, LEAFNODE_FAILURE])
    DECORATOR = DecoratorNode_NOT("DECORATOR_NOT", SEQUENCE_WITHMEMORY)

    DECORATOR.status = DECORATOR.run()

    bt_string_clear()
    for item in bt_string(bt_list(DECORATOR)):
        print(item)



